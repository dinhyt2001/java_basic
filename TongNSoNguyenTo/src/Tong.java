import java.util.Scanner;

public class Tong {
    public static void main(String[] args){
        int n, tong=0, count=0, x=2;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        while(true){
            if(laSNT(x)){
                tong=tong+x;
                count++;
            }
            if(count == n){
                break;
            }
            if(x==2) {x++;}
            else{ x+=2;}
        }
        System.out.println("Tong cua"+" "+n+" " +"SNT dau tien la:" + tong);
    }
    public static boolean laSNT(int n){
        for(int i=2;i<=Math.sqrt(n);i++){
            if(n%i == 0) return false;
        }
        return true;
    }
}
