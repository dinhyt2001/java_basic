package com.company;

public class ChuNhat {
    public double rong;
    public double dai;
    public double getChuVi(){
        double chuVi = (this.dai + this.rong) * 2;
        return chuVi;
    }
    public double getDienTich(){
        double dienTich = this.dai * this.rong;
        return dienTich;
    }
    public void xuat(){
        System.out.println("chieu dai:" + this.dai);
        System.out.println("chieu rong:" + this.rong);
        System.out.println("chu vi:" + this.getChuVi());
        System.out.println("dien tich:" + this.getDienTich());
    }
}
