package com.company;

public class PhoneNumber {
    String soDt;
    String nhaMang;
    String giaTien;
    public PhoneNumber(String soDt, String nhaMang, String giaTien){
        super();
        this.soDt = soDt;
        this.nhaMang = nhaMang;
        this.giaTien = giaTien;
    }
    public String getSoDt(){
        return soDt;
    }
    public String getNhaMang(){
        return nhaMang;
    }
    public String getGiaTien(){
        return giaTien;
    }
    public void setSoDt(String soDt){
        this.soDt = soDt;
    }
    public void setNhaMang(String nhaMang){
        this.nhaMang = nhaMang;
    }
    public void setGiaTien(String giaTien){
        this.giaTien = giaTien;
    }
    public String toString(){
        return "so dien thoai:" + soDt + ", nha mang:" + nhaMang + "gia tien:" + giaTien + ".";
    }
}
